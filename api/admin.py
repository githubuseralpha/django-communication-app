from django.contrib import admin

from api.models import Profile, Room, Message

admin.site.register(Profile)
admin.site.register(Room)
admin.site.register(Message)
