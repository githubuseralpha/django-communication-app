import random
import string

from django.contrib.auth import get_user_model
from django.db import models


def code_generator(size=20, chars=string.ascii_uppercase + string.digits):
    while True:
        code = ''.join(random.choice(chars) for _ in range(size))
        if Room.objects.filter(code=code).count() == 0:
            break
    return code


class RoomManager(models.Manager):
    def get_by_profile(self, profile_id):
        rooms = super().get_queryset().filter(users__id=profile_id).distinct()
        return rooms

    def get_by_owner(self, owner):
        rooms = super().get_queryset().filter(owner=owner)
        return rooms


class Profile(models.Model):
    user = models.OneToOneField(to=get_user_model(), on_delete=models.CASCADE)
    is_confirmed = models.BooleanField(default=False)


class Room(models.Model):
    name = models.CharField(max_length=24, default="server")
    owner = models.ForeignKey(to=Profile, on_delete=models.CASCADE)
    users = models.ManyToManyField(to=Profile, related_name="users")
    create_date = models.DateTimeField(auto_now_add=True)
    code = models.CharField(unique=True, max_length=32, default=code_generator)

    objects = RoomManager()


class Message(models.Model):
    owner = models.ForeignKey(to=Profile, on_delete=models.CASCADE)
    text = models.TextField()
    create_date = models.DateTimeField(auto_now_add=True)
    room = models.ForeignKey(to=Room, on_delete=models.CASCADE)
