from rest_framework import permissions

from api.models import Profile


class IsOwnerOrAdmin(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        NON_OWNER_METHODS = 'GET', 'HEAD', 'OPTIONS', 'POST'
        if request.method in NON_OWNER_METHODS:
            return True

        profile = Profile.objects.filter(user=request.user)[0]
        return obj.owner == profile or request.user.is_superuser


class IsMember(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        NON_MEMBER_METHODS = 'HEAD', 'OPTIONS', 'POST'
        if request.method in NON_MEMBER_METHODS:
            return True

        profile = Profile.objects.filter(user=request.user)[0]
        return obj.users.filter(id=profile.id).exists()


class IsAdminListPermission(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.user.is_superuser
