from rest_framework import serializers
from django.contrib.auth import get_user_model

from .models import Profile, Room, Message


class UserSerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField()

    class Meta:
        model = get_user_model()
        fields = ['id', 'username', 'password']


class ProfileSerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField()
    user = UserSerializer(read_only=True)

    class Meta:
        model = Profile
        fields = ["id", "user", "is_confirmed"]


class MessageCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Message
        fields = ["owner", "text", "room"]


class MessageSerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField()
    owner = ProfileSerializer(read_only=True)

    class Meta:
        model = Message
        fields = ["id", "owner", "text", "create_date", "room"]


class RoomSerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField()
    messages = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Room
        fields = ["id", "name", "owner", "users", "messages", "create_date", "code"]

    def get_messages(self, obj):
        msgs = Message.objects.filter(room=obj)
        return [MessageSerializer(msg).data for msg in msgs]



class RoomCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Room
        fields = ["name", "owner"]


class RoomRemoveSerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField()
    profile = serializers.ModelField(Profile)

    class Meta:
        model = Room
        fields = ["id", "profile"]


class RoomAddSerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField()

    class Meta:
        model = Room
        fields = ["id", "code"]
