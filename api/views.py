from rest_framework import viewsets, status
from django.contrib.auth import get_user_model
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, IsAuthenticatedOrReadOnly, IsAdminUser

from api.models import Profile, Message, Room
from api.permisssions import IsOwnerOrAdmin, IsMember, IsAdminListPermission
from api.serializers import ProfileSerializer, MessageSerializer, RoomSerializer, UserSerializer, \
    RoomRemoveSerializer, RoomAddSerializer, RoomCreateSerializer


class ProfileViewSet(viewsets.ModelViewSet):
    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer

    def get_permissions(self):
        if self.action == "list":
            self.permission_classes = [IsAdminListPermission]
        else:
            self.permission_classes = [IsAuthenticated]
        return super(ProfileViewSet, self).get_permissions()


class MessageViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated, IsOwnerOrAdmin]
    queryset = Message.objects.all()
    serializer_class = MessageSerializer

    def get_permissions(self):
        if self.action == "list":
            self.permission_classes = [IsAdminListPermission]
        else:
            self.permission_classes = [IsAuthenticated, IsOwnerOrAdmin]
        return super(MessageViewSet, self).get_permissions()

    def create(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        if not serializer.is_valid():
            return Response({'error': 'Invalid data.'}, status=status.HTTP_400_BAD_REQUEST)

        profile = Profile.objects.filter(user=self.request.user)
        if not profile.exists():
            return Response({'error': 'Software error - No profile created for that user'},
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        profile = profile[0]
        room = serializer.validated_data.get("room")
        if not room.users.filter(id=profile.id).exists():
            return Response({'error': 'Not permitted. '}, status=status.HTTP_403_FORBIDDEN)

        serializer.save(owner=profile)
        return Response({'error': None}, status=status.HTTP_201_CREATED)


class RoomViewSet(viewsets.ModelViewSet):
    queryset = Room.objects.all()
    serializer_class = RoomSerializer
    
    def create(self, request, *args, **kwargs):
        profile = Profile.objects.filter(user=self.request.user)
        if not profile.exists():
            return Response({'error': 'Software error - No profile created for that user'},
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        profile = profile[0]
        data = {
            **request.data,
            'owner': profile.id
        }
        serializer = RoomCreateSerializer(data=data)

        if not serializer.is_valid():
            return Response({'error': 'Invalid data.'}, status=status.HTTP_400_BAD_REQUEST)
        
        room = serializer.save(owner=profile, users=[profile])
        return Response({'id': room.id}, status=status.HTTP_201_CREATED)

    def get_permissions(self):
        if self.action == "list":
            self.permission_classes = [IsAuthenticated, IsAdminListPermission]
        elif self.action == 'add_profile':
            self.permission_classes = [IsAuthenticated]
        else:
            self.permission_classes = [IsOwnerOrAdmin, IsMember, IsAuthenticated]
        return super(RoomViewSet, self).get_permissions()

    def get_serializer_class(self):
        if self.action == 'remove_profile':
            return RoomRemoveSerializer
        if self.action == 'add_profile':
            return RoomAddSerializer
        return RoomSerializer

    @action(methods=['put'], detail=True,
            url_path='add-profile', url_name='add-profile')
    def add_profile(self, request, pk=None, *args, **kwargs):
        room = self.get_object()
        if room.code == request.data.get("code"):
            user = request.user
            try:
                profile = Profile.objects.filter(user=user)[0]
                if room.users.filter(id=profile.id).exists():
                    return Response({'error': 'That user already belong to this room'},
                                    status=status.HTTP_406_NOT_ACCEPTABLE)
                room.users.add(profile)
                return Response({'success': True, 'room':room.id}, status=status.HTTP_200_OK)
            except IndexError:
                return Response({'error': 'Software error - No profile created for that user'},
                                status=status.HTTP_406_NOT_ACCEPTABLE)
            except Exception as e:
                print(e)
                return Response({'error': 'Not recognized.'},
                                status=status.HTTP_406_NOT_ACCEPTABLE)
        else:
            return Response({'error': 'Invalid code'}, status=status.HTTP_406_NOT_ACCEPTABLE)

    @action(methods=['put'], detail=True,
            url_path='remove-profile', url_name='remove-profile')
    def remove_profile(self, request, pk=None, *args, **kwargs):
        room = self.get_object()
        profile = request.data.get("profile")
        room.users.remove(profile)
        return Response({'success': True}, status=status.HTTP_200_OK)

    @action(methods=['get'], detail=False,
            url_path='by-user', url_name='by-user')
    def rooms_by_user(self, request, pk=None, *args, **kwargs):
        user = request.user
        serializer = self.serializer_class
        try:
            profile = Profile.objects.filter(user=user)[0]
            queryset = Room.objects.get_by_profile(profile.id)
            serialized_data = serializer(queryset, many=True)
            return Response(serialized_data.data, status=status.HTTP_200_OK)
        except IndexError:
            return Response({'error': 'Software error - No profile created for that user'},
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        except Exception as e:
            print(e)
            return Response({'error': 'Not recognized error.'},
                            status=status.HTTP_406_NOT_ACCEPTABLE)


class UserViewSet(viewsets.ModelViewSet):
    queryset = get_user_model().objects.all()
    serializer_class = UserSerializer

    def get_permissions(self):
        if self.action == "list":
            self.permission_classes = [IsAdminListPermission]
        elif self.action == "create":
            self.permission_classes = []
        else:
            self.permission_classes = [IsAuthenticated]
        return super(UserViewSet, self).get_permissions()

    def perform_create(self, serializer):
        if not serializer.is_valid():
            return Response({'error': 'Invalid data.'}, status=status.HTTP_400_BAD_REQUEST)
        get_user_model().objects.create_user(username=serializer.data['username'],
                                             password=serializer.data['password'])
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    
    
    @action(methods=['get'], detail=False,
            url_path='is-auth', url_name='is-auth')
    def is_auth(self, request, pk=None, *args, **kwargs):
        return Response({'id': Profile.objects.filter(user=request.user.id)[0].id, 'username': request.user.username},
                            status=status.HTTP_200_OK)
