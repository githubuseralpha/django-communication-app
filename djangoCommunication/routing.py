from django.urls import re_path

from socketCommunication.consumers import ChatConsumer, VideoConsumer

websocket_urlpatterns = [
    re_path(r"^ws/room/chat/(?P<id>[\d])$", ChatConsumer.as_asgi()),
    re_path(r"^ws/room/video/(?P<id>[\d])$", VideoConsumer.as_asgi()),
]
