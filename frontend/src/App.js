import {BrowserRouter as Router, Route, Routes} from 'react-router-dom'
import {useEffect, useState} from 'react'
import './App.css';
import Servers from "./components/Servers";
import Login from "./components/Login";
import Register from "./components/Register";
import Home from "./components/Home";
import NavigationBar from "./components/Navbar";
import Server from "./components/Server";
import Invitation from './components/Invitation';
import CreateServer from './components/CreateServer';


function App() {
    const [isAuthenticated, setIsAuthenticated] = useState(false)
    const [username, setUsername] = useState("")
    const [userId, setUserId] = useState(-1)

    const serverDomain = "127.0.0.1:8000"
    const allowedForNotAuth = ["/login", "/register"]

    const checkAuthentication = async () => {       
        const requestOptions = {
            method: "GET",
            headers: {
                'Authorization': 'Token ' + localStorage.getItem("token")
            },
        };
        const endpoint = "api/users/is-auth"
        const domain = "http://" + serverDomain
        const url = domain + "/" + endpoint

        return fetch(url, requestOptions)
            .then((response) => {
                if(response.ok){
                    return response.json()
                }
                return false
            }).then((data) => {
                setUserId(data.id)
                setUsername(data.username)
                return true
            })
    }

    useEffect(() => {
        checkAuthentication().then((res) => {
            setIsAuthenticated(res)
            if(res==false && !allowedForNotAuth.includes(window.location.pathname)){
                window.location.href = "/login"
            }
        })
    }, [])

    return (
        <Router>
            <NavigationBar isAuthenticated={isAuthenticated}/>
            <Routes>
                <Route exact path='/' element={<Home />} />
                <Route path='/servers' element={<Servers serverDomain={serverDomain} userId={userId}/>}/>
                <Route path='/create-server' element={<CreateServer serverDomain={serverDomain}/>}/>
                <Route path='/login' element={<Login serverDomain={serverDomain}/>}/>
                <Route path='/register' element={<Register serverDomain={serverDomain}/>}/>
                <Route path='/server/:id' element={<Server serverDomain={serverDomain}/>}/>
                <Route path='/invitation/:code/:id' element={<Invitation serverDomain={serverDomain}/>}/>
            </Routes>
        </Router>
    );
}

export default App;
