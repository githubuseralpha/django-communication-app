import {useState} from 'react'
import {Alert, Button, Card, Col, Container, Form, Row} from 'react-bootstrap';


const CreateServer = () => {
    const [name, setName] = useState('')
    const [errorMsg, setErrorMsg] = useState('')
    const [validated, setValidated] = useState(false)

    const handleSubmit = async (event) => {
        event.preventDefault();
        if (event.currentTarget.checkValidity() === true) {
            const requestOptions = {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    'Authorization': 'Token ' + localStorage.getItem("token")
                },
                body: JSON.stringify({
                    name: name
                }),
            };
            const endpoint = "api/rooms/"
            const domain = "http://127.0.0.1:8000"
            const url = domain + "/" + endpoint
            fetch(url, requestOptions)
                .then((response) => {
                    if (response.ok) {
                        return response.json()
                    } else {
                        setErrorMsg('Error occured!')
                    }
                })
                .then((data) => {
                    if (data) {
                        window.location.href = "/server/" + data.id
                    }
                })
        } else {
            setValidated(true)
        }
    }

    const renderAlert = (msg, variant) => {
        return (
            <Alert variant={variant}>
                {msg}
            </Alert>
        )
    }

    return (
        <Container className="d-flex align-items-center justify-content-center" style={{height: "100vh"}}>
            <div className="create-server-box">
                <Col className="center">
                    <Row className="justify-content-center mt-4">
                        <h1> Create server </h1>
                    </Row>

                    <Row className="justify-content-center text-center">
                        <Form noValidate validated={validated} onSubmit={handleSubmit}>
                            {errorMsg && renderAlert(errorMsg, "danger")}
                            <Form.Group controlId="name" className="m-4">
                                <Form.Label>Server name</Form.Label>
                                <Form.Control type="text" placeholder="Enter server name" required
                                              onChange={(e) =>
                                                  setName(e.target.value)}/>
                            </Form.Group>

                            <Button variant="outline-light" type="submit" className="m-4">
                                Create server
                            </Button>
                        </Form>
                    </Row>
                </Col>
            </div>
        </Container>
    );
}

export default CreateServer;
