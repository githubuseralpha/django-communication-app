import {useEffect, useState} from 'react'
import {Button, Col, Container, Row} from "react-bootstrap";
import {BsChatFill, BsPeopleFill} from 'react-icons/bs';


const Home = () => {
    const onButtonClick = (loc) => {
        window.location.href = "/" + loc
    }

    return (
        <Container className="align-items-center justify-content-center"
                   style={{height: "100vh", width: "100vw"}} fluid>
            <Row>
                <Col className="m-0 p-0">
                    <Button style={{width: '50vw', height: '100vh'}} variant="dark"
                            onClick={() => onButtonClick("friends")}>
                        <BsPeopleFill className="m-2" size={50}/>
                        <h4> Friends </h4>
                    </Button>
                </Col>
                <Col className="m-0 p-0">
                    <Button style={{width: '50vw', height: '100vh'}} variant="dark"
                            onClick={() => onButtonClick("servers")}>
                        <BsChatFill className="m-2" size={50}/>
                        <h4> Servers </h4>
                    </Button>
                </Col>
            </Row>
        </Container>
    );
}

export default Home;
