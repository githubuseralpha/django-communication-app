import {useEffect} from 'react'
import {useParams} from "react-router";

const Invitation = (props) => {

    const {code, id} = useParams()

    useEffect(() => {
        const requestOptions = {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
                'Authorization': 'Token ' + localStorage.getItem("token")
            },
            body: JSON.stringify({
                code: code,
            }),
        };
        const endpoint = "api/rooms/" + id + "/add-profile/"
        const domain = "http://" + props.serverDomain
        const url = domain + "/" + endpoint
        fetch(url, requestOptions)
            .then((response) => {
                if(response.ok){
                    return response.json()
                }
            }).then((data)=>{
                window.location.href = window.location.origin + '/server/' + data.room
            })
    }, [])

    return (
        <></>
    );
}

export default Invitation;
