import {useEffect, useState} from 'react'
import {Alert, Button, Card, Col, Container, Form, Row} from 'react-bootstrap';
import {Link, useSearchParams} from "react-router-dom";


const Login = (props) => {
    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')
    const [errorMsg, setErrorMsg] = useState('')
    const [successMsg, setSuccessMsg] = useState('')
    const [validated, setValidated] = useState(false)

    const [searchParams, setSearchParams] = useSearchParams();


    useEffect(() => {
        if(searchParams.get("logged")){
            setSuccessMsg("Account created! Now you can log in")
        }
    }, [])

    const handleSubmit = async (event) => {
        event.preventDefault();
        if (event.currentTarget.checkValidity() === true) {
            const requestOptions = {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify({
                    username: username,
                    password: password,
                }),
            };
            const endpoint = "api/api-token-auth/"
            const domain = "http://" + props.serverDomain
            const url = domain + "/" + endpoint
            fetch(url, requestOptions)
                .then((response) => {
                    console.log(response)
                    if (response.ok) {
                        return response.json()
                    } else {
                        setSearchParams('')
                        setSuccessMsg('')
                        setErrorMsg('Wrong username or password!')
                    }
                })
                .then((data) => {
                    if (data) {
                        localStorage.setItem('token', data.token)
                        window.location.href = "/"
                    }
                })
        } else {
            setValidated(true)
        }
    }

    const renderAlert = (msg, variant) => {
        return (
            <Alert variant={variant}  className="ml-2 mr-2">
                {msg}
            </Alert>
        )
    }

    return (
        <Container className="d-flex align-items-center justify-content-center" style={{height: "100vh"}}>
            <div className="login-box">
                <Col className="center">
                    <Row className="justify-content-center mt-4">
                        <h1> Login </h1>
                    </Row>

                    <Row className="justify-content-center text-center">
                        <Form noValidate validated={validated} onSubmit={handleSubmit}>
                            {successMsg && renderAlert(successMsg, "success")}
                            {errorMsg && renderAlert(errorMsg, "danger")}
                            <Form.Group controlId="username" className="p-2">
                                <Form.Label>Username</Form.Label>
                                <Form.Control type="text" placeholder="Enter username" required
                                              onChange={(e) =>
                                                  setUsername(e.target.value)}/>
                            </Form.Group>

                            <Form.Group controlId="password" className="p-2">
                                <Form.Label>Password</Form.Label>
                                <Form.Control type="password" placeholder="Enter password" required
                                              onChange={(e) =>
                                                  setPassword(e.target.value)}/>
                            </Form.Group>

                            <Button variant="outline-light" type="submit" className="mb-2 mt-2">
                                Login
                            </Button>
                        </Form>
                        <p className="register-text mt-2 mb-3"> You don't have an account? <a href="/register"> Register now </a> </p>
                    </Row>
                </Col>
            </div>
        </Container>
    );
}

export default Login;
