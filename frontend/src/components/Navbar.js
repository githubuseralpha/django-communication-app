import {Navbar, Nav, NavDropdown} from 'react-bootstrap';
import {BsGearFill} from "react-icons/bs"

const NavigationBar = (props) => {

    const logout = () => {
        localStorage.setItem('token', '')
        window.location.href = "/login/"
    }

    return (
        <> { props.isAuthenticated ?
            <Navbar style={{backgroundColor: "rgba(0, 0, 0, 0)", width:"100px"}}
                    expand="lg" fixed="bottom" className="">
                <Nav>
                    <NavDropdown title={<a><BsGearFill size={25} color="white"/></a>} className="dropup">
                        <NavDropdown.Item onClick={ logout } href="">Logout</NavDropdown.Item>
                    </NavDropdown>
                </Nav>
            </Navbar> : <></>
        }
        </>
    )
}

export default NavigationBar
