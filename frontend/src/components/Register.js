import {useEffect, useState} from 'react'
import {Alert, Button, Card, Col, Container, Form, Row} from 'react-bootstrap';
import {Link} from "react-router-dom";


const Register = () => {
    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')
    const [confirmedPassword, setConfirmedPassword] = useState('')
    const [errorMsg, setErrorMsg] = useState('')
    const [validated, setValidated] = useState(false)

    const handleSubmit = async (event) => {
        event.preventDefault();
        if(confirmedPassword != password){
            setErrorMsg('Passwords does not match!')
            setValidated(false)
            return null
        }
        if (event.currentTarget.checkValidity() === true) {
            const requestOptions = {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify({
                    username: username,
                    password: password,
                }),
            };
            const endpoint = "api/users/"
            const domain = "http://127.0.0.1:8000"
            const url = domain + "/" + endpoint
            fetch(url, requestOptions)
                .then((response) => {
                    if (response.ok) {
                        return response.json()
                    } else {
                        console.log(response)
                        setErrorMsg('Username has to be unique!')
                    }
                })
                .then((data) => {
                    if (data) {
                        window.location.href = "/login?logged=1"
                    }
                })
        } else {
            setValidated(true)
        }
    }

    const renderAlert = (msg, variant) => {
        return (
            <Alert variant={variant}>
                {msg}
            </Alert>
        )
    }

    const handleConfirmedChange = (e) => {
        setConfirmedPassword(e.target.value)
    }

    return (
        <Container className="d-flex align-items-center justify-content-center" style={{height: "100vh"}}>
            <div className="login-box">
                <Col className="center">
                    <Row className="justify-content-center mt-4">
                        <h1> Register </h1>
                    </Row>

                    <Row className="justify-content-center text-center">
                        <Form noValidate validated={validated} onSubmit={handleSubmit}>
                            {errorMsg && renderAlert(errorMsg, "danger")}
                            <Form.Group controlId="username" className="p-2">
                                <Form.Label>Username</Form.Label>
                                <Form.Control type="text" placeholder="Enter username" required
                                              onChange={(e) =>
                                                  setUsername(e.target.value)}/>
                            </Form.Group>

                            <Form.Group controlId="password" className="p-2">
                                <Form.Label>Password</Form.Label>
                                <Form.Control type="password" placeholder="Enter password" required
                                              onChange={(e) =>
                                                  setPassword(e.target.value)}/>
                            </Form.Group>

                            <Form.Group controlId="password" className="p-2">
                                <Form.Label>Confirm password</Form.Label>
                                <Form.Control type="password" placeholder="Enter password" required
                                              onChange={(e) =>
                                                handleConfirmedChange(e)}/>
                            </Form.Group>

                            <Button variant="outline-light" type="submit" className="mb-2 mt-2">
                                Register
                            </Button>
                        </Form>
                        <p className="register-text mt-2 mb-3"> You already have an account? <a href="/login"> Login now </a> </p>
                    </Row>
                </Col>
            </div>
        </Container>
    );
}

export default Register;
