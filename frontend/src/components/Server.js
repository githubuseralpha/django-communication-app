import {useCallback, useEffect, useRef, useState, createRef} from 'react'
import {Button, Col, Container, Form, Row, Card} from "react-bootstrap";
import { BsNutFill } from 'react-icons/bs';
import {useParams} from "react-router";
import Webcam from "react-webcam";

const videoConstraints = {
    width: 320,
    height: 240,
    facingMode: "user"
};

const Server = (props) => {
    const [message, setMessage] = useState('')
    const [messages, setMessages] = useState([])
    const [images, setImages] = useState({})
    const [focused, setFocused] = useState('')

    const {id} = useParams()

    const domain = "ws://" + props.serverDomain
    const chatPath = "ws/room/chat/" + id
    const videoPath = "ws/room/video/" + id
    const chatEndpoint = domain + "/" + chatPath
    const videoEndpoint = domain + "/" + videoPath
    const FPS = 15

    let videoSocket = useRef(new WebSocket(videoEndpoint)).current
    let chatSocket = useRef(new WebSocket(chatEndpoint)).current
    const mainIm = useRef(null)
    const webcam = useRef(null);

    const capture = useCallback(
        () => {
            try{
                return webcam.current.getScreenshot()
            }
            catch(error){
                return null
            }
        },
        [webcam]
    );

    useEffect(() => {
        const scrollingElement = document.getElementById("test");
        scrollingElement.scrollTop = scrollingElement.scrollHeight;
        getServerInfo();
    }, [])


    chatSocket.onopen = (e) => {
        chatSocket.send(JSON.stringify({"token": localStorage.getItem("token")}))
    }

    function sleep(time) {
        return new Promise((resolve) => setTimeout(resolve, time));
    }

    videoSocket.onopen = (e) => {
        sleep(1500).then(() => {
            videoSocket.send(JSON.stringify({"token": localStorage.getItem("token")}))
            setInterval(() => {
                const data = {
                    "img": capture()
                }
                videoSocket.send(JSON.stringify(data))
            }, 1000 / FPS);
        });
    }


    chatSocket.onmessage = (e) => {
        const data = JSON.parse(e.data)
        const newMessage = {
            "username": data.username,
            "text": data.message
        }
        setMessages(messages.concat([newMessage]))
    }

    videoSocket.onmessage = (e) => {
        try{
            const data = JSON.parse(e.data)
            if (data.img){
                var newIms = images;
                if (data.username in images == false){
                    newIms[data.username] = newIms[data.username] || createRef();
                    setImages({...newIms});
                    console.log(images)
                }
                images[data.username].current.src = data.img
                if(focused == data.username){
                    mainIm.current.src = data.img
                }
            }
        }
        catch(error){
        }
    }

    const handleSubmit = (e) => {
        e.preventDefault()
        if(message != ''){
            const data = {'message': message}
            chatSocket.send(JSON.stringify(data))
            setMessage('')
        }
    }

    const getServerInfo = async () => {
        const requestOptions = {
            method: "GET",
            headers: {
                'Authorization': 'Token ' + localStorage.getItem("token")
            },
        };
        const path = "api/rooms/" + id
        const domain = "http://" + props.serverDomain
        const endpoint = domain + "/" + path

        fetch(endpoint, requestOptions)
            .then((response) => {
                if (response.ok) {
                    return response.json();
                }
            })
            .then((data) => {
                if (data) {
                    console.log(data)
                    let preprocessedMessages = []
                    for (let i = 0; i < data.messages.length; i++) {
                        preprocessedMessages.push(
                            {
                                "username": data.messages[i].owner.user.username,
                                "text": data.messages[i].text
                            })
                    }
                    setMessages(preprocessedMessages)
                }
            })
    }

    const chooseColor = (i) => {
        let color = "#363b41"
        if (i % 2 === 0)
            color = "#3C4249"
        return color
    }

    const preprocessMessage = (message) => {
        let messages = []
        let messageMod = message
        const maxLineLen = 30;
        while (messageMod.length > 0) {
            let j = Math.min(maxLineLen, messageMod.length)
            messages.push(messageMod.slice(0, j))
            messageMod = messageMod.slice(j)
        }
        return messages.join("\n ")
    }

    const renderMessageElement = (message, i) => {
        return (
            <Row style={{backgroundColor: chooseColor(i)}} className="p-2" key={i}>
                <span style={{color: "red"}}>{message.username}:&nbsp;</span>
                <span>{preprocessMessage(message.text)}</span>
            </Row>
        )
    }

    const renderTopVideoElement = (user, i) => renderVideoElement(user, images[user], i, "8rem")

    const renderVideoElement = (username, ref, i, size) => {
        return (
            <Col md="3" key={{i}}>
                <Card style={{ width: size, backgroundColor: "rgb(60,60,70)"}} onClick={() => changeFocus(username)}>
                    <Card.Img variant="top" ref={ ref }/>
                    <Card.Body className="card-body">
                        <Card.Text className="card-text">{ username }</Card.Text>
                    </Card.Body>
                </Card>
            </Col>
        )
    }

    const renderMainVideo = () => {
        return (
            <img ref={mainIm} style={{ width: "97%", margin: "auto" }}/>
        )
    }

    const changeFocus = (username) => {
        setFocused(username)
    }

    return (
        <Container fluid>
            <Webcam
                muted={false}
                audio={false}
                ref={webcam}
                screenshotFormat="image/jpeg"
                videoConstraints={videoConstraints}
                style={{
                    position: "fixed",
                    top: -400
                }}
            />
            <Row>
                <Col sm={6}>
                    <Row className="video-display">
                    {console.log('hmm')}
                    {Object.keys(images).map((value, index) => renderTopVideoElement(value, index))}
                    </Row>
                    <Row>
                        {renderMainVideo()}
                    </Row>
       
                </Col>
                <Col sm={6} className="chat-box align-self-end" id="test">
                    {messages.map((value, index) => renderMessageElement(value, index))}
                        <Row>
                            <Form  onSubmit={handleSubmit}>
                                <Row  className="m-0 ">
                                    <Col sm={11} style={{height: "100%"}} className="p-0 m-0">

                                        <Form.Control type="text"
                                                    placeholder="Enter message" value={message} onChange={(e) =>
                                            setMessage(e.target.value)}
                                        />
                                    </Col>
                                    <Col sm={1}  className="m-0 p-0">
                                        <Button variant="primary" type="submit"
                                                style={{height: "100%"}}>
                                                    >
                                        </Button>
                                    </Col>
                                </Row>
                            </Form>
                        </Row>
                </Col>
            </Row>
        </Container>
    );
}

export default Server;
