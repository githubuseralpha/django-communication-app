import {useEffect, useState} from 'react'
import {Button, Col, Row, Offcanvas} from 'react-bootstrap';
import {AiOutlinePlus, AiOutlineUserAdd, AiOutlineLink,
     AiOutlineArrowRight, AiOutlineDelete, AiOutlineUserDelete} from 'react-icons/ai';

const Servers = (props) => {
    const [serverList, setServerList] = useState([])
    const [hidden, setHidden] = useState(true);
    const [serverToRemove, setServerToRemove] = useState(-1);


    useEffect(() => {
        getServerList()
    }, [])

    const getServerList = async () => {
        const requestOptions = {
            method: "GET",
            headers: {
                'Authorization': 'Token ' + localStorage.getItem("token")
            },
        };
        const endpoint = "api/rooms/by-user"
        const domain = "http://" + props.serverDomain
        const url = domain + "/" + endpoint

        fetch(url, requestOptions)
            .then((response) => {
                if (response.ok) {
                    return response.json();
                } else {
                    console.log(response)
                }
            })
            .then((data) => {
                if (data) {
                    setServerList(data)
                    console.log(data)
                }
            })
    }

    const deleteServer = () => {
        setHidden(true)
        setServerList(serverList.filter((data) => data.id != serverToRemove))
        const requestOptions = {
            method: "DELETE",
            headers: {
                'Authorization': 'Token ' + localStorage.getItem("token")
            },
        };
        const endpoint = "api/rooms/" + serverToRemove
        const domain = "http://" + props.serverDomain
        const url = domain + "/" + endpoint

        fetch(url, requestOptions)
        .then((response) => {
            if (response.ok) {
                console.log(response)
            } else {
                console.log(response)
            }
        })
    }

    const handleButtonClick = (id) => {
        window.location.href = "/server/" + id
    }

    const handleInvitationCopy = (code, id) => {
        console.log(window.location)
        const invitationLink = window.location.origin + '/invitation/' + code + '/' + id
        navigator.clipboard.writeText(invitationLink);
    }

    const handleDeleteButton = (serverId) => {
        if(hidden==true){
            setServerToRemove(serverId)
            setHidden(false)
        }
    }

    const renderOwnerButtons = (server) => {
        return (
        <>
            <Button style={{ height: '3rem', margin: '10px' }} variant="outline-danger">
                <AiOutlineUserDelete size={24}/>
            </Button>
            <Button style={{ height: '3rem', margin: '10px' }} variant="outline-danger"
                onClick={()=> handleDeleteButton(server.id)}>
                <AiOutlineDelete size={24}/>
            </Button>
        </>
        )
    }

    const renderServerElement = (server, index) => {
        return (
            <Row className="justify-content-center text-center mt-1" key={index}>
                <div className='server-div'>
                    <h3 className='mb-3'>{server.name}</h3>
                    <Button style={{ height: '3rem', margin: '10px' }} variant="outline-light"
                        onClick={()=>handleButtonClick(server.id)}>
                        <AiOutlineArrowRight size={24}/>
                    </Button>
                    <Button style={{ height: '3rem', margin: '10px' }} variant="outline-light">
                        <AiOutlineUserAdd size={24}/>
                    </Button>
                    <Button style={{ height: '3rem', margin: '10px' }} variant="outline-light"
                    onClick={() => handleInvitationCopy(server.code, server.id)}>
                        <AiOutlineLink size={24}/>
                    </Button>
                    {props.userId == server.owner && renderOwnerButtons(server)}
                </div>
            </Row>
        )
    }

    const renderDeletePopover = () => {
        return (
            <div hidden={hidden} className='delete-popover'>
                <h3>Remove server</h3>
                <p>Are you sure you want to remove this server?</p>
                <Button variant='outline-danger' className='m-2'
                onClick={()=>deleteServer()}>
                    Yes
                </Button>
                <Button variant='outline-success' className='m-2'
                onClick={()=>setHidden(true)}>
                    No
                </Button>
            </div>
        )
    }

    return (
        <>   
            {renderDeletePopover()}
            <Col className="center mt-4 pt-2">
                <Row className="justify-content-center">
                    <h1 className="m-5 p-4" style={{color: "white"}}> YOUR SERVERS </h1>
                </Row>
                <Row className="justify-content-center text-center mt-1 mb-3">
                    <Button style={{width: '45vw', height: '10rem'}} variant="dark"
                    onClick={()=>{window.location.href = '/create-server'}}>
                        <h3 style={{color:"rgb(150,150,150)", marginBottom: "0px"}}>
                            <AiOutlinePlus size={50}/> Create server
                        </h3>
                    </Button>
                </Row>
                {serverList.map((value, index) => {
                    return renderServerElement(value, index)
                })}
            </Col>
        </>
    );
}

export default Servers;
