from django.db.models.signals import post_save, pre_delete
from django.dispatch import receiver
from django.contrib.auth import get_user_model

from api.models import Profile


@receiver(post_save, sender=get_user_model())
def create_profile(sender, instance, created, **kwargs):
    if instance and created:
        Profile.objects.create(user=instance)


@receiver(pre_delete, sender=get_user_model())
def delete_profile(sender, instance, **kwargs):
    profile = Profile.objects.filter(user=instance)[0]
    profile.delete()
