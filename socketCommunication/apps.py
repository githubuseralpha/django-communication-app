from django.apps import AppConfig


class SocketcommunicationConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'socketCommunication'
