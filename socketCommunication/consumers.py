import json

from channels.consumer import AsyncConsumer
from channels.db import database_sync_to_async
from rest_framework.authtoken.models import Token

from api.models import Room, Profile
from api.serializers import MessageCreateSerializer


class BaseConsumer(AsyncConsumer):
    @database_sync_to_async
    def get_room(self, pk):
        room = Room.objects.filter(id=pk)
        if room.exists():
            return room[0]
        return None

    @database_sync_to_async
    def get_user(self, key):
        if key:
            token = Token.objects.filter(key=key)
            if token.exists():
                token = token[0]
                return token.user
        return None

    async def has_permission(self):
        profile = Profile.objects.filter(user=self.user)
        if profile.exists():
            room = await self.get_room(self.room_id)
            if room:
                if room.users.filter(profile.id).exists():
                    return True
        return False

    async def _websocket_connect(self, room_name):
        self.room_id = self.scope["url_route"]["kwargs"]["id"]
        self.room = f"{room_name}_{self.room_id}"
        self.user = None
        await self.channel_layer.group_add(
            self.room,
            self.channel_name
        )
        await self.send({
            "type": "websocket.accept",
        })

    async def _websocket_receive(self, event):
        text = event.get("text")
        if text:
            dict_data = json.loads(text)
            if self.user is None:
                token = dict_data.get("token")
                self.user = await self.get_user(token)
                return None
            else:
                return dict_data

    async def websocket_disconnect(self, event):
        pass


class ChatConsumer(BaseConsumer):
    async def websocket_connect(self, event):
        await self._websocket_connect("chat_room")

    async def websocket_receive(self, event):
        dict_data = await self._websocket_receive(event)
        if dict_data:
            if self.has_permission():
                message = dict_data.get("message")
                response = {
                    "username": self.user.username,
                    "message": message
                }
                await self.create_message(message)
                await self.channel_layer.group_send(
                    self.room,
                    {
                        "type": "chat_message",
                        "text": json.dumps(response)
                    })

    async def chat_message(self, event):
        await self.send({
            "type": "websocket.send",
            "text": event["text"]
        })

    @database_sync_to_async
    def create_message(self, message):
        profile = Profile.objects.filter(user=self.user)
        if profile.exists():
            profile = profile[0]
            data = {
                "owner": profile.id,
                "text": message,
                "room": self.room_id
            }
            serializer = MessageCreateSerializer(data=data)
            if serializer.is_valid(raise_exception=True):
                serializer.save()


class VideoConsumer(BaseConsumer):
    async def websocket_connect(self, event):
        await self._websocket_connect("video_room")

    async def websocket_receive(self, event):
        dict_data = await self._websocket_receive(event)
        if dict_data:
            if self.has_permission():
                response = {
                    "username": self.user.username,
                    "img": dict_data.get("img")
                }
                await self.channel_layer.group_send(
                    self.room,
                    {
                        "type": "image_message",
                        "text": json.dumps(response)
                    })

    async def image_message(self, event):
        await self.send({
            "type": "websocket.send",
            "text": event["text"]
        })
